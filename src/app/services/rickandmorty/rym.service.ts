import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RymService {

  public URI = 'https://rickandmortyapi.com/api/';

  constructor(private _http : HttpClient) { }

  getPersonajes() {
    const url =`${this.URI}/character`;
    return this._http.get(url);
  }
}
